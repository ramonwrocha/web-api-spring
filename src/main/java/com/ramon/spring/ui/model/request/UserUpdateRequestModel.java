package com.ramon.spring.ui.model.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserUpdateRequestModel {
	@NotNull(message = "firstName cannot be null")
	@Size(min = 2, max = 50, message = "firstName cannot less then 2 chars and cannot grater 50 chars")
	private String firstName;

	@Size(min = 2, max = 50, message = "lastName cannot less then 2 chars and cannot grater 50 chars")
	@NotNull(message = "lastName cannot be null")
	private String lastName;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}	
	
}
