package com.ramon.spring.service;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ramon.spring.service.interfaces.IUserService;
import com.ramon.spring.shared.Utils;
import com.ramon.spring.ui.model.request.UserDetailsRequestModel;
import com.ramon.spring.ui.model.response.UserRest;

@Service
public class UserService implements IUserService {

	Map<String, UserRest> users;
	
	Utils utils;
	
	public UserService() {}

	@Autowired
	public UserService(Utils utils) {
		this.utils = utils;
	}

	@Override
	public UserRest createUser(UserDetailsRequestModel model) {
		UserRest user = new UserRest();
		String id = utils.generateUserID();
		user.setId(id);
		user.setEmail(model.getEmail());
		user.setFirstName(model.getFirstName());
		user.setLastName(model.getLastName());

		if (users == null)
			users = new HashMap<>();

		users.put(id, user);

		return user;
	}

}
