package com.ramon.spring.service.interfaces;

import com.ramon.spring.ui.model.request.UserDetailsRequestModel;
import com.ramon.spring.ui.model.response.UserRest;

public interface IUserService {	
	UserRest createUser(UserDetailsRequestModel model);
}
