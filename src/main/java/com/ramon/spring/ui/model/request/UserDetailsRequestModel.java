package com.ramon.spring.ui.model.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserDetailsRequestModel {
	@NotNull(message = "firstName cannot be null")
	@Size(min = 2, max = 50, message = "firstName cannot less then 2 chars and cannot grater 50 chars")
	private String firstName;

	@Size(min = 2, max = 50, message = "lastName cannot less then 2 chars and cannot grater 50 chars")
	@NotNull(message = "lastName cannot be null")
	private String lastName;

	@NotNull(message = "email cannot be null")
	@Email(message = "email not valid")
	private String email;

	@NotNull(message = "password cannot be null")
	@Size(min = 8, max = 16, message = "password cannot less then 8 and cannot grater 16")
	private String password;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
