package com.ramon.spring.exceptions;

public class UserServiceException extends RuntimeException {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2937366134650982540L;

	public UserServiceException(String message) {
		super(message);
	}
}
